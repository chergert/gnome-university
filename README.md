# GNOME University

For the most up to date information on GNOME University, please see
https://live.gnome.org/GnomeUniversity

## Dependencies

 * dia
 * texlive
 * texlive-cm-super
 * texlive-iwona
 * texlive-pdflatex
 * texlive-pdftools
 * texlive-pdftools-bin
 * texlive-tetex-bin-bin

```sh
sudo yum install dia texlive texlive-cm-super texlive-iwona texlive-pdflatex texlive-pdftools texlive-pdftools-bin texlive-tetex-bin-bin
```

## Building

If building from git, do the typical:

```sh
./autogen.sh
make
```

If you would like to install the files:

```sh
sudo make install
```

They will be installed into ${prefix}/share/gnome-university.
